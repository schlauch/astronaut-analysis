#!/bin/bash
# SPDX-FileCopyrightText: 2024 German Aerospace Center (DLR)
# SPDX-License-Identifier: CC0-1.0

# === Configuration section ===
# - BRANCH that marks the main development and should be used for re-creation of branches
BRANCH="main"

# - name of REMOTE repository to sync the result to.
REMOTE="origin"

# - stages contains the branch names that should be created.
# - stage_offsets reference the commits relative to the selected BRANCH tip.
#   -> stages[i] will be set to track $BRANCH~stage_offsets[i]
stages=(
  "6-release-code"
  "5-prepare-citation"
  "4-add-license-information"
  "3-add-documentation"
  "2-clean-up-code"
  "1-put-into-git"
  "0-original-source"
)
stage_offsets=(1 2 3 5 6 11 13)
# === End of Configuration section ===

# Utility variables (used for counting etc.)
NUM_COMMITS=$(git rev-list --count $BRANCH)
NUM_COMMITS_OFFSET="$(($NUM_COMMITS-14))"

# Check that we are on the right branch
CURR_BRANCH=$(git branch --show-current)
if [ "$BRANCH" != "$CURR_BRANCH" ]; then
  echo "error: this script must be run in a git repo with checked out branch $BRANCH"
  exit 1
fi

# Correctly associate commit and reference branches
STAGE_C=${#stages[@]}

printf "Reset %s branches (with --force)? (y/N):" "$STAGE_C"
read -r answer
if [ "$answer" != "${answer#[Yy]}" ] ;then 
  for i in $(seq 0 $((STAGE_C-1)))
  do
    echo "Pointing branch '${stages[$i]}' to commit '${BRANCH}~${stage_offsets[$i]}'."
	STAGE_OFFSET="$((${stage_offsets[$i]}+$NUM_COMMITS_OFFSET))"
	COMMIT=$(git rev-parse --short "${BRANCH}~${STAGE_OFFSET}")
    git branch --force "${stages[$i]}" "$COMMIT"
    git branch --set-upstream-to=${REMOTE}/"${stages[$i]}" "${stages[$i]}"
  done
fi

# Push remote tracking branches
printf "Push %s branches with --force? (y/N):" "$STAGE_C"
read -r answer
if [ "$answer" != "${answer#[Yy]}" ] ;then 
  for i in $(seq 0 $((STAGE_C-1)))
  do
    git push --force $REMOTE "${stages[$i]}"
  done
fi

# Correct the release tag
RELEASE_TAG="2024-03-20"
printf "Apply and push tag '%s' to stage '%s' (with --force)? (y/N):" "$RELEASE_TAG" "${stages[0]}"
read -r answer
if [ "$answer" != "${answer#[Yy]}" ] ;then 
  git tag --force --message="Mark the initial release" --annotate "$RELEASE_TAG" "${stages[0]}"
  git push --force origin "$RELEASE_TAG"
fi
