# SPDX-FileCopyrightText: 2018 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: Apache-2.0

"""The standalone script analyzes the astronaut data sets."""

import datetime

import matplotlib.pyplot as plt
import pandas as pd

ASTRONAUT_DATA_RETRIEVAL_DATE = datetime.date.fromisoformat("2018-10-25")
_ASTRONAUT_DATA_FILE = "../data/astronauts.json"


##
# Data cleanup functions
##
def cleanup_data_set(astronauts_df):
    """Clean up and prepare data set for further analysis."""
    # Rename columns and set the index
    astronauts_df = rename_columns(astronauts_df)
    astronauts_df = astronauts_df.set_index("astronaut_id")

    # Set pandas dtypes for columns with date or time
    astronauts_df = astronauts_df.dropna(subset=["time_in_space"])
    astronauts_df["time_in_space"] = astronauts_df["time_in_space"].astype(int)
    astronauts_df["time_in_space"] = pd.to_timedelta(
        astronauts_df["time_in_space"],
        unit="m",
    )
    astronauts_df["birthdate"] = pd.to_datetime(astronauts_df["birthdate"])
    astronauts_df["date_of_death"] = pd.to_datetime(astronauts_df["date_of_death"])
    astronauts_df = astronauts_df.sort_values("birthdate")

    # Calculate extra columns from the original data
    astronauts_df["time_in_space_D"] = (
        astronauts_df["time_in_space"].dt.total_seconds() / 86400
    )
    astronauts_df["alive"] = astronauts_df["date_of_death"].apply(is_alive)
    astronauts_df["age"] = astronauts_df["birthdate"].apply(calculate_age)
    astronauts_df["died_with_age"] = astronauts_df.apply(
        calculate_died_with_age,
        axis=1,
    )
    return astronauts_df


def rename_columns(astronauts_df):
    """Improve naming of the data set columns."""
    name_mapping = {
        "astronaut": "astronaut_id",
        "astronautLabel": "name",
        "birthplaceLabel": "birthplace",
        "sex_or_genderLabel": "sex_or_gender",
    }
    return astronauts_df.rename(index=str, columns=name_mapping)


def is_alive(date_of_death):
    """Determine whether the person is still alive."""
    if pd.isna(date_of_death):
        return True
    return False


def calculate_age(birth_date):
    """Determine the current age on the basis of the given date of birth."""
    return _caculate_age(birth_date, ASTRONAUT_DATA_RETRIEVAL_DATE)


def calculate_died_with_age(row):
    """Determine the age of a person that died."""
    if pd.isna(row["date_of_death"]):
        return None
    if pd.isna(row["birthdate"]):
        return None
    return _caculate_age(row["birthdate"], row["date_of_death"])


def _caculate_age(birth_date, today):
    # Convert 'birth_date' to a date object if it is a Timestamp
    if isinstance(birth_date, pd.Timestamp):
        birth_date = birth_date.date()

    # Convert 'today' to a date object if it is a Timestamp
    if isinstance(today, pd.Timestamp):
        today = today.date()
    if birth_date > today:
        return None
    return (
        today.year
        - birth_date.year
        - (1 if (today.month, today.day) < (birth_date.month, birth_date.day) else 0)
    )


##
# Plot functions
##
def create_humans_in_space_plot(astronauts_df):
    """Generate a plot with the summed up time of humans
    in space over the years by their birthday's.
    """
    return _create_time_of_x_in_space_plot(
        astronauts_df,
        "Total time humans spent in space",
    )


def create_males_in_space_plot(astronauts_df):
    """Generate a plot with the summed up time of males
    in space over the years by their birthday's.
    """
    male_astronauts_df = astronauts_df.loc[
        astronauts_df["sex_or_gender"] == "male",
        ["birthdate", "time_in_space", "time_in_space_D"],
    ]
    return _create_time_of_x_in_space_plot(
        male_astronauts_df,
        "Total time males spent in space",
    )


def create_females_in_space_plot(astronauts_df):
    """Generate a plot with the summed up time of males
    in space over the years by their birthday's.
    """
    female_astronauts_df = astronauts_df.loc[
        astronauts_df["sex_or_gender"] == "female",
        ["birthdate", "time_in_space", "time_in_space_D"],
    ]
    return _create_time_of_x_in_space_plot(
        female_astronauts_df,
        "Total time females spent in space",
    )


def _create_time_of_x_in_space_plot(astronauts_df, title):
    """Generate the humans in space plots."""
    # Prepare data set
    reduced_df = astronauts_df[["birthdate", "time_in_space", "time_in_space_D"]].copy()
    reduced_df["accumulated_time_in_days"] = reduced_df["time_in_space_D"].cumsum()

    # Create plot
    plt.style.use("ggplot")
    axs = reduced_df.plot(x="birthdate", y="accumulated_time_in_days")
    axs.set_title(title)
    axs.set_xlabel("Years")
    axs.set_ylabel("t in days")
    return axs.get_figure()


def create_age_histogram_plot(astronauts_df):
    """Generate a combined histogram of the astronaut's age distribution
    for the categories 'dead' and 'alive'.
    """
    # Select data
    died_df = astronauts_df.loc[astronauts_df["alive"] == 0, ["died_with_age"]]
    age_df = astronauts_df.loc[astronauts_df["alive"] == 1, ["age"]]

    # Create plot
    plt.style.use("ggplot")
    figure, axs = plt.subplots(1, 1)
    axs.hist(
        [died_df["died_with_age"], age_df["age"]],
        bins=70,
        range=(31, 100),
        stacked=True,
    )
    axs.set_xlabel("Age")
    axs.set_ylabel("Number of astronauts")
    axs.set_title("Age distribution of dead and alive astronauts")
    return figure


def create_age_box_plot(astronauts_df):
    """Generate a box plot of astronaut's age distribution
    for the categories 'dead' and 'alive'.
    """
    # Select data
    died_df = astronauts_df.loc[astronauts_df["alive"] == 0, ["died_with_age"]]
    age_df = astronauts_df.loc[astronauts_df["alive"] == 1, ["age"]]

    # Create plot
    plt.style.use("ggplot")
    figure, axs = plt.subplots(1, 1)
    axs.boxplot([died_df["died_with_age"], age_df["age"]])
    axs.set_xlabel("Category")
    plt.setp(axs, xticks=[1, 2], xticklabels=["Dead", "Alive"])
    axs.set_ylabel("Age")
    axs.set_title("Age distribution of dead and alive astronauts")
    return figure


def perform_analysis():
    """Glue data preparation and plotting."""
    # Load and clean up data
    astronauts_df = pd.read_json(_ASTRONAUT_DATA_FILE)
    astronauts_df = cleanup_data_set(astronauts_df)

    # Create humans in space plots
    # Male humans in space
    male_in_space_plot = create_males_in_space_plot(astronauts_df)
    male_in_space_plot.savefig("male_humans_in_space.png")

    # Female humans in space
    female_in_space_plot = create_females_in_space_plot(astronauts_df)
    female_in_space_plot.savefig("female_humans_in_space.png")

    # Humans in space plot
    humans_in_space_plot = create_humans_in_space_plot(astronauts_df)
    humans_in_space_plot.savefig("humans_in_space.png")

    # Create age distribution plots
    # Combined histogram of dead and alive astronauts
    age_histogram_plot = create_age_histogram_plot(astronauts_df)
    age_histogram_plot.savefig("combined_histogram.png")

    # Box plot of dead vs. alive astronauts
    age_box_plot = create_age_box_plot(astronauts_df)
    age_box_plot.savefig("boxplot.png")


# Main entry point
if __name__ == "__main__":
    perform_analysis()
