#!/bin/bash
# SPDX-FileCopyrightText: 2018 German Aerospace Center (DLR)
# SPDX-License-Identifier: Apache-2.0


# Exit when any command fails
set -e

# Install required dependencies
pip install -r requires/requirements.txt
echo "Successfully installed required packages"

# Run tests
pytest astronaut_analysis_test.py --junitxml=pytest.xml
echo "Successfully ran tests"

# Check the code using the ruff linter
ruff check --select ALL --ignore PTH,T,PLR,ANN,D205 --output-file=ruff.json --output-format=gitlab astronaut_analysis.py
echo "Successfully ran ruff checks"

# Check that copyright and license information for all files is available
reuse --root ../ lint

# Check that the script is basically working and creating the same results
python astronaut_analysis.py
test -f boxplot.png
test -f combined_histogram.png
test -f female_humans_in_space.png
test -f humans_in_space.png
test -f male_humans_in_space.png
echo "Successfully created the plots"
